"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
function phoneValidator(phoneNumber) {
    var valid = /^(\+0?1\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/.test(phoneNumber.value);
    if (valid) {
        return null;
    }
    return { "invalidPhoneNumber": true };
}
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var customer_service_1 = require('./customer-service');
var router_1 = require('@angular/router');
var CustomerForm = (function () {
    function CustomerForm(customerService, router) {
        this.customerService = customerService;
        this.router = router;
        this.payLoad = null;
        var group = {};
        group.name = new forms_1.FormControl('', forms_1.Validators.required);
        group.companyName = new forms_1.FormControl('', forms_1.Validators.required);
        group.phoneNumber = new forms_1.FormControl('', phoneValidator);
        group.emailAddress = new forms_1.FormControl('');
        this.form = new forms_1.FormGroup(group);
    }
    CustomerForm.prototype.onSubmit = function () {
        var _this = this;
        this.payLoad = JSON.stringify(this.form.value);
        this.customerService
            .createCustomer(this.form.value)
            .subscribe(function (obj) {
            var localobj = JSON.stringify(obj.json());
            _this.customerService.storeLocally(localobj);
            _this.router.navigateByUrl('/auction');
        });
    };
    CustomerForm = __decorate([
        core_1.Component({
            selector: 'customer-form',
            directives: [forms_1.REACTIVE_FORM_DIRECTIVES],
            templateUrl: './components/customer/customer-form.html',
        }), 
        __metadata('design:paramtypes', [customer_service_1.CustomerService, router_1.Router])
    ], CustomerForm);
    return CustomerForm;
}());
exports.CustomerForm = CustomerForm;
//# sourceMappingURL=customer-form.js.map