export class Customer {

    name              :   string;
    companyName       :   string;
    phoneNumber       :   string;
    emailAddress      :   string;
    id                :   string;
    
    
    constructor(name:string,companyName:string,phoneNumber:string,emailAddress:string,id:string) {
        this.id=id;
        this.name=name;
        this.companyName=companyName;
        this.phoneNumber=phoneNumber;
        this.emailAddress=emailAddress;
    }


}