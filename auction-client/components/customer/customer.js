"use strict";
var Customer = (function () {
    function Customer(name, companyName, phoneNumber, emailAddress, id) {
        this.id = id;
        this.name = name;
        this.companyName = companyName;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
    }
    return Customer;
}());
exports.Customer = Customer;
//# sourceMappingURL=customer.js.map