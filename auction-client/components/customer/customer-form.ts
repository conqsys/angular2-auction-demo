function phoneValidator(phoneNumber:any) {

    var valid = /^(\+0?1\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/.test(phoneNumber.value);
    if(valid) {
        return null;
    }

    return {"invalidPhoneNumber":true};
}

import {Component} from '@angular/core';
import {FormGroup, Validators, FormControl, REACTIVE_FORM_DIRECTIVES} from '@angular/forms';
import {CustomerService} from './customer-service';
import {Http} from '@angular/http';
import {Customer} from './customer'; 
import {Router} from '@angular/router'


@Component({
    selector: 'customer-form',
    directives:[REACTIVE_FORM_DIRECTIVES],
    templateUrl: './components/customer/customer-form.html',
    
})

export class CustomerForm {

    form:any;
    payLoad:any = null;

    constructor(private customerService:CustomerService,private router:Router) {
        let group:any = {};
        group.name =  new FormControl('', Validators.required);
        group.companyName = new FormControl('', Validators.required);
        group.phoneNumber = new FormControl('', phoneValidator);
        group.emailAddress = new FormControl('');

        this.form = new FormGroup(group);
    }

    onSubmit() {
        this.payLoad = JSON.stringify(this.form.value);
        this.customerService
            .createCustomer(this.form.value)
            .subscribe(obj=> {
                var localobj=JSON.stringify(obj.json());                
                this.customerService.storeLocally(localobj);
                this.router.navigateByUrl('/auction');
            })
    }
}