import {Http,Response, Headers} from '@angular/http';
import {Injectable} from '@angular/core';
import {ApiUrls} from '../common/apiurls';
import {Customer} from './customer';
import {Observable} from 'rxjs/Observable';
import {LocalStorage} from "angular2-localstorage/WebStorage";

@Injectable()
export class CustomerService {

  @LocalStorage()
  public customerString:any;

  



 
  constructor( private http:Http){
    console.log(this.customerString);
  }

  createCustomer(customerData) {
    
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return  this.http
            .post(ApiUrls.CreateCustomer(),customerData ,{headers:headers})

  }

  Customer() {
    return JSON.parse(this.customerString);
  }


  

  storeLocally(customer) {
    this.customerString=customer;
  }
}