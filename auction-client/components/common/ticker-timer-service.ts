import {Http,Response, Headers} from '@angular/http';
import {Injectable} from '@angular/core';
import {ApiUrls} from '../common/apiurls'



export class TickerTimerService {
  constructor(private http: Http) {}

  getAuctionExpireTime() {
    return  this.http
    .get(ApiUrls.GetAllDeals())                
    .map((res) => res.json())
  }

}