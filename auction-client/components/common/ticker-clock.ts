import {Component,OnInit} from '@angular/core'
import * as moment  from '../../node_modules/moment/moment'
import {Moment}  from '../../node_modules/moment/moment'
import {AuctionService} from "../auction/auction-service"
import {Auction} from "../auction/auction"
import {Router} from '@angular/router'

@Component({
    selector:'ticker-clock',
    template:`<div style="margin-left: 20px;margin-bottom: 10px;">
                <h4>{{DiffTime }}</h4>                
                </div>`                
})

export class TickerClock implements OnInit {
    OpenAuction:Auction;
    DiffTime:any;
    clock:any;    
    constructor(private auctionService:AuctionService,private router:Router) {        
                
    }

    ngOnInit() {
        this.OpenAuction=new Auction(new Date(Date.now()),new Date(Date.now()+1000000));                
        this.startClock();
        this.auctionService
            .getOpenOrCreateAuction()
            .subscribe(res=> {
                let auction=res.json();
                console.log(auction);
                this.OpenAuction=new Auction(auction.startDateTime,auction.endDateTime,auction.auctionId);
            })
    }

    startClock () {
        this.clock=setInterval(()=> 
        {             
            let now=new Date(Date.now());                                    
            let diffTime:number= (now.getHours()*3600)+(now.getMinutes()*60)+now.getSeconds();
            diffTime=this.OpenAuction.endTimeInSeconds-diffTime;

            if(diffTime<0) {
                clearInterval(this.clock);
                //this.router.navigateByUrl('/auctionresult');                
            }

            let hours:any   = Math.floor(diffTime / 3600);
            let minutes:any = Math.floor((diffTime - (hours * 3600)) / 60);
            let seconds:any = diffTime - (hours * 3600) - (minutes * 60);

            if (hours   < 10) {hours   = "0"+hours.toString();}
            if (minutes < 10) {minutes = "0"+minutes;}
            if (seconds < 10) {seconds = "0"+seconds;}
            this.DiffTime=hours+':'+minutes+':'+seconds; 
           
            
        },1000)
    }
}