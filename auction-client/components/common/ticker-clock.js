"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var auction_service_1 = require("../auction/auction-service");
var auction_1 = require("../auction/auction");
var router_1 = require('@angular/router');
var TickerClock = (function () {
    function TickerClock(auctionService, router) {
        this.auctionService = auctionService;
        this.router = router;
    }
    TickerClock.prototype.ngOnInit = function () {
        var _this = this;
        this.OpenAuction = new auction_1.Auction(new Date(Date.now()), new Date(Date.now() + 1000000));
        this.startClock();
        this.auctionService
            .getOpenOrCreateAuction()
            .subscribe(function (res) {
            var auction = res.json();
            console.log(auction);
            _this.OpenAuction = new auction_1.Auction(auction.startDateTime, auction.endDateTime, auction.auctionId);
        });
    };
    TickerClock.prototype.startClock = function () {
        var _this = this;
        this.clock = setInterval(function () {
            var now = new Date(Date.now());
            var diffTime = (now.getHours() * 3600) + (now.getMinutes() * 60) + now.getSeconds();
            diffTime = _this.OpenAuction.endTimeInSeconds - diffTime;
            if (diffTime < 0) {
                clearInterval(_this.clock);
            }
            var hours = Math.floor(diffTime / 3600);
            var minutes = Math.floor((diffTime - (hours * 3600)) / 60);
            var seconds = diffTime - (hours * 3600) - (minutes * 60);
            if (hours < 10) {
                hours = "0" + hours.toString();
            }
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }
            _this.DiffTime = hours + ':' + minutes + ':' + seconds;
        }, 1000);
    };
    TickerClock = __decorate([
        core_1.Component({
            selector: 'ticker-clock',
            template: "<div style=\"margin-left: 20px;margin-bottom: 10px;\">\n                <h4>{{DiffTime }}</h4>                \n                </div>"
        }), 
        __metadata('design:paramtypes', [auction_service_1.AuctionService, router_1.Router])
    ], TickerClock);
    return TickerClock;
}());
exports.TickerClock = TickerClock;
//# sourceMappingURL=ticker-clock.js.map