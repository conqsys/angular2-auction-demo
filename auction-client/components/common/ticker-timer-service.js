"use strict";
var apiurls_1 = require('../common/apiurls');
var TickerTimerService = (function () {
    function TickerTimerService(http) {
        this.http = http;
    }
    TickerTimerService.prototype.getAuctionExpireTime = function () {
        return this.http
            .get(apiurls_1.ApiUrls.GetAllDeals())
            .map(function (res) { return res.json(); });
    };
    return TickerTimerService;
}());
exports.TickerTimerService = TickerTimerService;
//# sourceMappingURL=ticker-timer-service.js.map