import {Http,Response, Headers} from '@angular/http';
import {Injectable} from '@angular/core';
import {ApiUrls} from '../common/apiurls';
import {Deal} from './deal';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class AuctionService {

  constructor(private http: Http) {}

  getDeals() {

    return  this.http
            .get(ApiUrls.GetAllDeals())                
            .map((res) => res.json())
                
  }

  submitBid(deal:Deal,auctionId:String,customerId:String) {
    let bid = {  
      auctionId:auctionId,
      customerId:customerId,
      dealId:deal.dealId,
      bidValue:deal.bidValue
    }

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return  this.http
            .post(ApiUrls.SubmitBid(auctionId),bid,{headers:headers})

  }

  updateBid(deal:Deal,auctionId:String) {
    let bid = {  
      bidId:deal.bidId,
      bidValue:deal.bidValue
    }

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return  this.http
            .post(ApiUrls.UpdateBid(auctionId,bid.bidId),bid,{headers:headers})

  }

  getOpenOrCreateAuction() {
    return  this.http
            .post(ApiUrls.GetOpenOrCreateAuction(),null);
  }
}