import {Component,ViewChild} from '@angular/core';
import {AuctionService} from './auction-service';
import {CustomerService} from '../customer/customer-service';
import {Deal} from './deal';
import { MODAL_DIRECTIVES,ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import {Observable} from 'rxjs/Observable';

@Component({
    selector: 'auction-submit',    
    templateUrl: './components/auction/auction-submit.html',
    directives: [MODAL_DIRECTIVES],
    providers:[AuctionService]
})

export class AuctionSubmit {
    
    @ViewChild('modal')
    modal                   :   ModalComponent;

    private deals           :   Array<Deal>;
    
    private selectedDeal    :   Deal;
    private customer        :   any;
    private auction         :   any;
    
    constructor(private auctionService:AuctionService,private customerService:CustomerService) {
        this.auctionService=auctionService; 
        this.getDeals();
        
        this.auctionService.getOpenOrCreateAuction().subscribe(obj=>this.auction=obj.json());
        this.customer=this.customerService.Customer()
    }

    getDeals() {
        return this.auctionService
                .getDeals()
                .subscribe(res=> {
                    this.deals=new Array<Deal>();
                    res.forEach((deal:any)=> {
                        this.deals.push(new Deal(deal.dealId,deal.dealName))                                                
                    })                    
                    console.log(this.deals);
                 });
    }

    submit(deal:Deal) {
        
        
        if(!deal.bidId || deal.bidId==null || deal.bidId.length===0) {
            console.log('submitting bid')
            console.log(deal);

            this.auctionService
                .submitBid(deal,this.auction.id,this.customer.id)
                .subscribe((res:any)=>{            
                    var bid=res.json();
                    deal.isSubmitted=true;
                    deal.bidSubmittedValue=deal.bidValue;
                    deal.bidId=bid.id;
                    console.log(JSON.stringify(deal));
	            })   
        } else {
            
            this.auctionService
                .updateBid(deal,this.auction.id)
                .subscribe((res:any)=> {
                    deal.isSubmitted=true;
                    deal.bidSubmittedValue=deal.bidValue;
                })
        }        
    }

    onChange(deal:any,newValue:Number) {        
        deal.bidValue=newValue;                
        if(deal.bidSubmittedValue!=deal.bidValue) {
            deal.isSubmitted=false;
        } else if (deal.bidValue.length>0) {
            deal.isSubmitted=true;
        }         
    }

    dismissSubmitConfirmDialog() {
        this.modal.close();
    }

    openSubmitConfirmDialog(deal:Deal) { 
        this.selectedDeal=deal;       
        this.modal.open();
    }
    
    submitConfirmDialogClosed() {
        this.submit(this.selectedDeal);
        this.modal.close();
    }


    
}