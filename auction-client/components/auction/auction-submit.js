"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var auction_service_1 = require('./auction-service');
var customer_service_1 = require('../customer/customer-service');
var deal_1 = require('./deal');
var ng2_bs3_modal_1 = require('ng2-bs3-modal/ng2-bs3-modal');
var AuctionSubmit = (function () {
    function AuctionSubmit(auctionService, customerService) {
        var _this = this;
        this.auctionService = auctionService;
        this.customerService = customerService;
        this.auctionService = auctionService;
        this.getDeals();
        this.auctionService.getOpenOrCreateAuction().subscribe(function (obj) { return _this.auction = obj.json(); });
        this.customer = this.customerService.Customer();
    }
    AuctionSubmit.prototype.getDeals = function () {
        var _this = this;
        return this.auctionService
            .getDeals()
            .subscribe(function (res) {
            _this.deals = new Array();
            res.forEach(function (deal) {
                _this.deals.push(new deal_1.Deal(deal.dealId, deal.dealName));
            });
            console.log(_this.deals);
        });
    };
    AuctionSubmit.prototype.submit = function (deal) {
        if (!deal.bidId || deal.bidId == null || deal.bidId.length === 0) {
            console.log('submitting bid');
            console.log(deal);
            this.auctionService
                .submitBid(deal, this.auction.id, this.customer.id)
                .subscribe(function (res) {
                var bid = res.json();
                deal.isSubmitted = true;
                deal.bidSubmittedValue = deal.bidValue;
                deal.bidId = bid.id;
                console.log(JSON.stringify(deal));
            });
        }
        else {
            this.auctionService
                .updateBid(deal, this.auction.id)
                .subscribe(function (res) {
                deal.isSubmitted = true;
                deal.bidSubmittedValue = deal.bidValue;
            });
        }
    };
    AuctionSubmit.prototype.onChange = function (deal, newValue) {
        deal.bidValue = newValue;
        if (deal.bidSubmittedValue != deal.bidValue) {
            deal.isSubmitted = false;
        }
        else if (deal.bidValue.length > 0) {
            deal.isSubmitted = true;
        }
    };
    AuctionSubmit.prototype.dismissSubmitConfirmDialog = function () {
        this.modal.close();
    };
    AuctionSubmit.prototype.openSubmitConfirmDialog = function (deal) {
        this.selectedDeal = deal;
        this.modal.open();
    };
    AuctionSubmit.prototype.submitConfirmDialogClosed = function () {
        this.submit(this.selectedDeal);
        this.modal.close();
    };
    __decorate([
        core_1.ViewChild('modal'), 
        __metadata('design:type', ng2_bs3_modal_1.ModalComponent)
    ], AuctionSubmit.prototype, "modal", void 0);
    AuctionSubmit = __decorate([
        core_1.Component({
            selector: 'auction-submit',
            templateUrl: './components/auction/auction-submit.html',
            directives: [ng2_bs3_modal_1.MODAL_DIRECTIVES],
            providers: [auction_service_1.AuctionService]
        }), 
        __metadata('design:paramtypes', [auction_service_1.AuctionService, customer_service_1.CustomerService])
    ], AuctionSubmit);
    return AuctionSubmit;
}());
exports.AuctionSubmit = AuctionSubmit;
//# sourceMappingURL=auction-submit.js.map