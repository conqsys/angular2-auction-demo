"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var auction_realtime_service_1 = require('./auction-realtime-service');
var AuctionRealtimeTicker = (function () {
    function AuctionRealtimeTicker(tickerService) {
        this.tickerService = tickerService;
        this.bids = [];
    }
    AuctionRealtimeTicker.prototype.sendMessage = function () {
        //this.tickerService.sendMessage({wow:'wow'});
        //this.message = '';
    };
    AuctionRealtimeTicker.prototype.ngOnInit = function () {
        var _this = this;
        this.connection = this.tickerService.getBids().subscribe(function (bid) {
            console.log(bid);
            _this.bids.push(bid);
        });
        //this.tickerService.doSomething();
    };
    AuctionRealtimeTicker.prototype.ngOnDestroy = function () {
        this.connection.unsubscribe();
    };
    AuctionRealtimeTicker = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'auction-realtime-ticker',
            templateUrl: '/components/auction/auction-realtime-ticker.html',
            providers: [auction_realtime_service_1.AuctionRealtimeService]
        }), 
        __metadata('design:paramtypes', [auction_realtime_service_1.AuctionRealtimeService])
    ], AuctionRealtimeTicker);
    return AuctionRealtimeTicker;
}());
exports.AuctionRealtimeTicker = AuctionRealtimeTicker;
//# sourceMappingURL=auction-realtime-ticker.js.map