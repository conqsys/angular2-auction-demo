"use strict";
var Deal = (function () {
    function Deal(dealId, dealName) {
        this.bidValue = 0;
        this.bidSubmittedValue = 0;
        this.isSubmitted = false;
        this.bidId = null;
        this.dealId = dealId;
        this.dealName = dealName;
    }
    return Deal;
}());
exports.Deal = Deal;
//# sourceMappingURL=deal.js.map