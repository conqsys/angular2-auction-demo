export class Deal {

    dealId              :   Number;
    dealName            :   String;
    bidValue            :   Number      = 0;
    bidSubmittedValue   :   Number      = 0;
    isSubmitted         :   Boolean     = false;
    bidId               :   String      = null;
    
    constructor(dealId:Number,dealName:string) {
        this.dealId=dealId;
        this.dealName=dealName;
    }


}
