export class Auction {

    auctionId            :   String;
    startTime            :   Date;
    endTime              :   Date;
    isOpen               :   Boolean;
    endTimeInSeconds     :   number;

    constructor(startTime,endTime,auctionId?) {
      
      console.log(endTime);
      this.startTime=new Date(Date.parse(startTime));
      this.endTime=new Date(Date.parse(endTime));
      this.isOpen=true;
      this.auctionId=auctionId;

      this.endTimeInSeconds= (this.endTime.getHours()*3600)+(this.endTime.getMinutes()*60)+this.endTime.getSeconds();

    } 

  
}    