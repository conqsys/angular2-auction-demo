"use strict";
var Observable_1 = require('rxjs/Observable');
//import {io}  from './node_modules/socket.io-client/socket.io';
var io = require('/node_modules/socket.io-client/socket.io');
var sails_io = require('/node_modules/sails.io.js/sails.io');
var AuctionRealtimeService = (function () {
    function AuctionRealtimeService() {
        this.url = 'http://192.168.1.89:5001';
    }
    AuctionRealtimeService.prototype.doSomething = function () {
        this.socket = io(this.url);
        console.log(this.socket);
        this.socket.on('bid', function (data) {
            //console.log(data);            
        });
    };
    AuctionRealtimeService.prototype.getBids = function () {
        var _this = this;
        var observable = new Observable_1.Observable(function (observer) {
            _this.socket = io(_this.url);
            console.log(_this.socket);
            _this.socket.on('bid', function (data) {
                console.log(data);
                observer.next(data);
            });
            return function () {
                _this.socket.disconnect();
            };
        });
        return observable;
    };
    return AuctionRealtimeService;
}());
exports.AuctionRealtimeService = AuctionRealtimeService;
//# sourceMappingURL=auction-realtime-service.js.map