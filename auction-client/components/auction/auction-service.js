"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var http_1 = require('@angular/http');
var core_1 = require('@angular/core');
var apiurls_1 = require('../common/apiurls');
var AuctionService = (function () {
    function AuctionService(http) {
        this.http = http;
    }
    AuctionService.prototype.getDeals = function () {
        return this.http
            .get(apiurls_1.ApiUrls.GetAllDeals())
            .map(function (res) { return res.json(); });
    };
    AuctionService.prototype.submitBid = function (deal, auctionId, customerId) {
        var bid = {
            auctionId: auctionId,
            customerId: customerId,
            dealId: deal.dealId,
            bidValue: deal.bidValue
        };
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        return this.http
            .post(apiurls_1.ApiUrls.SubmitBid(auctionId), bid, { headers: headers });
    };
    AuctionService.prototype.updateBid = function (deal, auctionId) {
        var bid = {
            bidId: deal.bidId,
            bidValue: deal.bidValue
        };
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        return this.http
            .post(apiurls_1.ApiUrls.UpdateBid(auctionId, bid.bidId), bid, { headers: headers });
    };
    AuctionService.prototype.getOpenOrCreateAuction = function () {
        return this.http
            .post(apiurls_1.ApiUrls.GetOpenOrCreateAuction(), null);
    };
    AuctionService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], AuctionService);
    return AuctionService;
}());
exports.AuctionService = AuctionService;
//# sourceMappingURL=auction-service.js.map