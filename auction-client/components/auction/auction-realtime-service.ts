import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
//import {io}  from './node_modules/socket.io-client/socket.io';
var io=require('/node_modules/socket.io-client/socket.io')
var sails_io = require('/node_modules/sails.io.js/sails.io');

export class AuctionRealtimeService {
  private url = 'http://192.168.1.89:5001';  
  private socket:any;
 

  doSomething(){
    this.socket = io(this.url);
      console.log(this.socket);
      this.socket.on('bid', (data:any) => {
        //console.log(data);            
      });
  }

  getBids() {

    let observable = new Observable((observer:any) => {
      this.socket = io(this.url);
      console.log(this.socket);
      this.socket.on('bid', (data:any) => {
        console.log(data);
        observer.next(data);    
      });
       return () => {
         this.socket.disconnect();
       };  
    })

    return observable;
  }  
}