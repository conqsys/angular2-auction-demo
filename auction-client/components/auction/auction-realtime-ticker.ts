import { Component, OnInit,OnDestroy } from '@angular/core';
import { Control }  from '@angular/common';
import { AuctionRealtimeService } from './auction-realtime-service';

@Component({
  moduleId: module.id,
  selector: 'auction-realtime-ticker',  
  templateUrl:'/components/auction/auction-realtime-ticker.html',
  providers: [AuctionRealtimeService]
})

export class AuctionRealtimeTicker implements OnInit, OnDestroy {
  bids:any[] = [];
  connection:any;
  message:any;
  
  constructor(private tickerService:AuctionRealtimeService) {}

  sendMessage(){
    //this.tickerService.sendMessage({wow:'wow'});
    //this.message = '';
  }

  ngOnInit() {
    this.connection = this.tickerService.getBids().subscribe((bid) => {
        console.log(bid);
      this.bids.push(bid);
    })

    //this.tickerService.doSomething();
    
  }
  
  ngOnDestroy() {
    this.connection.unsubscribe();
  }
}