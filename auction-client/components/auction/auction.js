"use strict";
var Auction = (function () {
    function Auction(startTime, endTime, auctionId) {
        console.log(endTime);
        this.startTime = new Date(Date.parse(startTime));
        this.endTime = new Date(Date.parse(endTime));
        this.isOpen = true;
        this.auctionId = auctionId;
        this.endTimeInSeconds = (this.endTime.getHours() * 3600) + (this.endTime.getMinutes() * 60) + this.endTime.getSeconds();
    }
    return Auction;
}());
exports.Auction = Auction;
//# sourceMappingURL=auction.js.map