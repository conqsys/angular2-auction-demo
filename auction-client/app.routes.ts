import {provideRouter, RouterConfig} from '@angular/router';

import {CustomerForm} from './components/customer/customer-form';
import {AuctionSubmit} from './components/auction/auction-submit';
import {AuctionRealtimeTicker} from './components/auction/auction-realtime-ticker';

export const routes: RouterConfig = [
  {
    path: '',
    redirectTo: '/customer',
    terminal: true
  },
  { path: 'customer', component: CustomerForm},
  { path: 'auction', component: AuctionSubmit},
  { path: 'ticker', component: AuctionRealtimeTicker},
  
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];

