"use strict";
var router_1 = require('@angular/router');
var customer_form_1 = require('./components/customer/customer-form');
var auction_submit_1 = require('./components/auction/auction-submit');
var auction_realtime_ticker_1 = require('./components/auction/auction-realtime-ticker');
exports.routes = [
    {
        path: '',
        redirectTo: '/customer',
        terminal: true
    },
    { path: 'customer', component: customer_form_1.CustomerForm },
    { path: 'auction', component: auction_submit_1.AuctionSubmit },
    { path: 'ticker', component: auction_realtime_ticker_1.AuctionRealtimeTicker },
];
exports.APP_ROUTER_PROVIDERS = [
    router_1.provideRouter(exports.routes)
];
//# sourceMappingURL=app.routes.js.map