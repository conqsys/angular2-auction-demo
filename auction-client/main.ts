import {HTTP_PROVIDERS} from '@angular/http';
import {Component} from '@angular/core';
import {bootstrap} from '@angular/platform-browser-dynamic';
import {provideForms, disableDeprecatedForms} from '@angular/forms';
import {ROUTER_DIRECTIVES} from '@angular/router';

import {HashLocationStrategy, LocationStrategy, Location} from '@angular/common';

import { APP_ROUTER_PROVIDERS } from './app.routes';
import {TickerClock} from './components/common/ticker-clock'
import {AuctionService} from './components/auction/auction-service'
import {CustomerService} from './components/customer/customer-service'
import {LocalStorageService, LocalStorageSubscriber,LocalStorageEmitter} from 'angular2-localstorage/LocalStorageEmitter';

declare var System:any;

@Component(
{
    selector: 'main-app',
    templateUrl: './main-app.html',
    directives:[ROUTER_DIRECTIVES,TickerClock],
    

})

 
class MyDemoApp {

    public customer:any;

    constructor(public location: Location,private customerService: CustomerService) {
        this.customer={name:''};
        LocalStorageEmitter.subscribe(()=>  {
            
            this.customer=this.customerService.Customer();
            if(this.customerService.customerString==null)
                this.customer={name:''};
            
        })
    }
    
}


var appPromise=bootstrap(MyDemoApp,[
          provideForms(),
          disableDeprecatedForms(),
          CustomerService,          
          AuctionService,          
          APP_ROUTER_PROVIDERS,
          LocalStorageService,
          {provide: LocationStrategy, useClass: HashLocationStrategy},
          HTTP_PROVIDERS]);


LocalStorageSubscriber(appPromise);