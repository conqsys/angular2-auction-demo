"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var http_1 = require('@angular/http');
var core_1 = require('@angular/core');
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var common_1 = require('@angular/common');
var app_routes_1 = require('./app.routes');
var ticker_clock_1 = require('./components/common/ticker-clock');
var auction_service_1 = require('./components/auction/auction-service');
var customer_service_1 = require('./components/customer/customer-service');
var LocalStorageEmitter_1 = require('angular2-localstorage/LocalStorageEmitter');
var MyDemoApp = (function () {
    function MyDemoApp(location, customerService) {
        var _this = this;
        this.location = location;
        this.customerService = customerService;
        this.customer = { name: '' };
        LocalStorageEmitter_1.LocalStorageEmitter.subscribe(function () {
            _this.customer = _this.customerService.Customer();
            if (_this.customerService.customerString == null)
                _this.customer = { name: '' };
        });
    }
    MyDemoApp = __decorate([
        core_1.Component({
            selector: 'main-app',
            templateUrl: './main-app.html',
            directives: [router_1.ROUTER_DIRECTIVES, ticker_clock_1.TickerClock],
        }), 
        __metadata('design:paramtypes', [common_1.Location, customer_service_1.CustomerService])
    ], MyDemoApp);
    return MyDemoApp;
}());
var appPromise = platform_browser_dynamic_1.bootstrap(MyDemoApp, [
    forms_1.provideForms(),
    forms_1.disableDeprecatedForms(),
    customer_service_1.CustomerService,
    auction_service_1.AuctionService,
    app_routes_1.APP_ROUTER_PROVIDERS,
    LocalStorageEmitter_1.LocalStorageService,
    { provide: common_1.LocationStrategy, useClass: common_1.HashLocationStrategy },
    http_1.HTTP_PROVIDERS]);
LocalStorageEmitter_1.LocalStorageSubscriber(appPromise);
//# sourceMappingURL=main.js.map