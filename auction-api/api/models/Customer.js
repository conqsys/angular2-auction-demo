module.exports = {

   attributes: {
       
        name: {
            type: 'string' 
        },
        companyName: {
            type: 'string', 
            defaultsTo: 1
        },
        phoneNumber: {
            type: 'string'
        },
        emailAddress: {
            type: 'string', 
            globalIndex: true, 
            indexName: 'emailAddress',   
            hashKey: true          
        },
        createdAt: {
            type:'date'
        },
        modifiedAt :{
            type:'date'
        } 
    }
};

