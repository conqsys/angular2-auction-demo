module.exports = {

   attributes: {       
			dealName: {
					type: 'string' 
			},
			dealId: {
					type: 'integer', 
					defaultsTo: 1
			}
	 },

	 seedData:function (){
		 
		 
		 	return new Promise((resolve,reject) => {
				Deal.create({dealName:'Deal 1',dealId:1}).exec((err,obj)=> { 
					Deal.create({dealName:'Deal 2',dealId:2}).exec(()=>{
						Deal.create({dealName:'Deal 3',dealId:3}).exec(()=>{
							Deal.create({dealName:'Deal 4',dealId:4}).exec(()=>{
								Deal.create({dealName:'Deal 5',dealId:5}).exec(()=>{
									Deal.create({dealName:'Deal 6',dealId:6}).exec(()=>{
										Deal.create({dealName:'Deal 7',dealId:7}).exec(()=>{
											resolve()
										})
									})	
								})			
							})
						})
					})													
			 	})
			 }) 
		 	
			 
	 } 

}