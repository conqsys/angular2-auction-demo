module.exports = {

   attributes: {       
			auctionId: {
				type: 'string' 
			},
			customerId :{
				type :'string'
			},
			bidValue: {
				type :'integer'
			},
			dealId: {
				type :'string'
			},
			companyName: {
				type :'string'
			},
			ipAddress: {
				type :'string'
			},
			country:{
				type :'string'
			}
	 }
}