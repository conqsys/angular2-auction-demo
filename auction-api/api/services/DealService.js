var AWS = require("aws-sdk");
AWS.config.update(sails.config.connections.dynamoDb);

var service={};

service.findAll=function() {
  return new Promise((resolve,reject) => {
    var docClient = new AWS.DynamoDB.DocumentClient();
      var params = {
        TableName: "deal"			
      };

      docClient.scan(params,(err,data) => {
        if(err) reject(err);
        else {
          if(data.Count===0) {
              Deal.seedData().then(()=> {
                reject('send request again')                
              });
          } else {
              resolve(data.Items);
          }
        }			
      });
  })
}

module.exports=service;