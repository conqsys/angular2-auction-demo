var service={};

service.create=function () {

  return new Promise((resolve,reject)=> {
    Auction.create({
      startDateTime : new Date(),
      endDateTime:new Date(Date.now()+(5*60*1000)),
      isOpen :1
    })
    .exec((err,obj)=>{
      if(err) 
        reject(err);
      else 
        resolve(obj);			
    });
  });
} 

service.findOpen=function () {
  return new Promise((resolve,reject)=> {  
    Auction
    .findOne({where:{isOpen:1}})
    .exec((err,obj)=> {
      if(err) reject(err);
      else 
        if(obj.items.length>0) {
          var auction=obj.items[0];
          if(Date.parse(auction.endDateTime)<Date.now()) {
            reject('auction closed');
          } else 
              resolve(obj.items[0]);
        } else {
          reject('No Open Auction');
        }	
    })
  });  
}

service.findOpenOrCreate=function () {
  return new Promise((resolve,reject)=> {  
    service.findOpen()
            .then(obj=> resolve(obj))
            .catch(err=> service.create()
                          .then(obj=>resolve(obj))
                          .error(err=>reject(err)))
  });  

}

service.updateBid=function(bid) {
  return new Promise((resolve,reject)=> {  
    Bid.update({where:{id:bid.bidId}},
		{				
				bidValue:parseFloat(bid.bidValue),				
		})
		.exec((err,obj)=>{
			if(err) reject(err);
			else {
				service.broadcastBid(obj);
        resolve(obj);				
			}
		})
  });  
}

service.submitNewBid=function (bid) {
  return new Promise((resolve,reject)=> {
    CustomerService
      .findById(bid.customerId)
      .then(customer=>{      
          Bid.create({
            auctionId:bid.auctionId,
            customerId:bid.customerId,
            bidValue:bid.bidValue,
            dealId:bid.dealId,
            companyName:customer.items[0].companyName
          })
          .exec((err,obj)=> {
            if(err) reject(err);
            else {
              service.broadcastBid(obj);
              resolve(obj);
            }
          });			
      })
      .catch(err=>reject(err))});          
}
service.broadcastBid=function (bid) {
  sails.io.emit('bid',bid);
}
module.exports=service;

  


