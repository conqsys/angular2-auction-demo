var service={};
service.findById=function (id) {
  return new Promise((resolve,reject)=> {
    Customer
			.findOne({where : {id:id} })
			.exec((err,obj) => {
					if(err)  reject(err);
					else resolve(obj)			
			});
  });  
}


service.create=function (customerData) {
  return new Promise((resolve,reject)=> {
    Customer.create({
        name:customerData.name,
        companyName:customerData.companyName,
        phoneNumber:customerData.phoneNumber,
        emailAddress:customerData.emailAddress
    })
    .exec((err,obj)=> {
        if(err) reject(err);
        else resolve(obj);
    })  
  })      
}

module.exports=service;