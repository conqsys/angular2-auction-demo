
module.exports = {    
	create : function (req,res) {
        var customerData=req.body;
        CustomerService.create(customerData)
                       .then(obj=> res.send(obj))
                       .catch(err=> res.negotiate(err)); 
       
    },

    getCustomer : function (req,res) {
        var id=req.params.id;
        if(!id) {
            return res.negotiate('Invalid Customer Id');            
        }
        CustomerService.findById(id)
                       .then(obj=> res.send(obj))
                       .catch(err=> res.negotiate(err)); 
    },

    getAllCustomers : function (req,res) {
        Customer
        .find({where :{customerName :'a'}})
        .exec((err,obj) => {
            if(err) {
                return res.negotiate(err);
            } else {
                return res.send(obj);
            }
        })
    },
    
};

