
module.exports = {

	startAuction : function (req,res){
		AuctionService.create()
									.then(obj=>res.send(obj))
									.catch(err=>res.negotiate(err));
	},

	openOrCreateAuction :function (req,res) {
		AuctionService.findOpenOrCreate()
									.then(obj=>res.send(obj))
									.catch(err=>res.negotiate(err));
	},

	submitNewBid : function (req,res) {
		var bid=req.body;
		AuctionService.submitNewBid(bid)
									.then(obj=>res.send(obj))
									.catch(err=>res.negotiate(err));		
	},

	updateBid : function (req,res) {
		var bid=req.body;
		AuctionService.updateBid(bid)
									.then(obj=>res.send(obj))
									.catch(err=>res.negotiate(err));
		
	},

		
}